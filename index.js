const app = require('express')()
const http = require('http').createServer(app)
const io = require('socket.io')(http);

let perguntas = ['Digite o endereço?','Qual sua idade?','Informe seu e-mail','Qual sua Profissão?'];
let respostas = [];
let cont = 0; 

app.get('/', (_, res) => {
  res.sendFile(`${__dirname}/index.html`)
})

io.on('connection', socket => {
    socket.on('newMessage', msg => {
        msg2= perguntas[cont];          
        io.emit('messageResponse', msg)      
        io.emit('messageResponse', msg2)
        respostas[cont] = msg;    
        if(cont==4){
          let cadastro = "Sua ficha cadastral ficou assim <br>Nome: "+respostas[0]+"<br>Endereço:"+respostas[1]+"<br> Idade: "+respostas[2]+"anos"+" <br>Email: "+respostas[3]+"<br> Profissão"+respostas[4];       
          io.emit('messageResponse', cadastro) ;
        }
        cont+=1;
    })
})
http.listen(3000, () => console.log('listening on *:3000'))